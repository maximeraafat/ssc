---
cooperation-id: 2
title: DBSSE
modal-id: dbsse

img: dbsse.jpg
alt: DBSSE
date_:

description:
---
Handover to our ETH team at the biosystems department in Basel!

Students: Anna Knörr, Nina Blaimscheim  
Professors: Prof. Sven Panker  
