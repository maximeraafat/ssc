---
cooperation-id: 1
title: DPHYS
modal-id: dphys

img: dphys.jpg
alt: DPHYS
date_:

description:
---
Handover to professors at the physics department on Hönggerberg!

Students: Luna Bloin-Wibe, Chrysander Hagen, Anna Knörr, Jan Zibell  
Professors: Prof. Jêrome Faist, Prof. Tilman Esslinger  
