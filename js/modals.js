function openModalOnHash() {
  if(window.location.hash) {
    var hash = window.location.hash.substring(1);
    $('#'+hash).modal('show');
  }
}

$(document).ready(function() {
  openModalOnHash()
});
