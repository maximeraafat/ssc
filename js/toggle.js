function toggle_news() {
  var x = document.getElementById("toggler_news");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function toggle_sust_eth() {
  var x = document.getElementById("toggler_sust_eth");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function toggle_daily() {
  var x = document.getElementById("toggler_daily");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function toggle_freetime() {
  var x = document.getElementById("toggler_freetime");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function toggle_sust_inf() {
  var x = document.getElementById("toggler_sust_inf");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function toggle_book() {
  var x = document.getElementById("toggler_book");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}

function toggle_past_semesters() {
  var x = document.getElementById("toggler_past_semesters");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
