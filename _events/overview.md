---
title: 2021 Fall Overview
event-id: 999
modal-id: overview

img: 2021_fall_overview.png
alt: overview

date_: 
time: 
location: 

description: 
---

Here's an overview of all SSC events we've planned for you in Fall 2021! Looking forward to seeing you there :)

If you're interested in what events took place in previous semesters, check out our Events Archive below!
