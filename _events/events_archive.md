---
title: Archive
event-id: 1
modal-id: events_archive

img: events_archive.png
alt: events_archive


description: 
---

Interested in some SSC history? Take a look at events organized in previous semesters: <a href="https://polybox.ethz.ch/index.php/s/xmdJW8kxqZKAgaH" target="_blank"><i>Polybox</i></a>
