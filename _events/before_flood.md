---
title: Movie Screening - Before the Flood
event-id: 1
modal-id: before_flood

img: before_flood.png
alt: Movie screening poster for Before the Flood

date_: 27.09.2021
time: "18:30 - 20.30"
location: HG F33.1

description:
---

Do you want to get to know the people behind the Student Sustainability Commission (SSC) better and what SSC stands for? Come to our first event of the semester and learn about SSC, our projects, teams and visions for the future. After our introduction we will watch the movie Before The Flood, in which you will discover through the voice of Leonardo di Caprio how climate change affects our environment and what society can do to prevent the demise of endangered species, ecosystems, and native communities across the planet.
