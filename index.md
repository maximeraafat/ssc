---
layout: default
---
{% include header.html %}
{% include about.html %}
{% include vanillaCalendar.html %}

<!-- Calendar Form JavaScript -->
<script src="/js/vanillaCalendar.js"></script>
