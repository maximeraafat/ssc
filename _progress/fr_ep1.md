---
title: A long way to Asia...
progress-id: 3
modal-id: flight_ep1

img: fl_red_ep1.png
alt: flight reduction episode 1
date_:

author: Theo Ray, Hasan Arkay and Julia Werz
description:
---
Going against the mainstream is not easy. That's why we decided to share the stories of people who decided to not fly, were creative and got to their destination in alternative fashion.

Dive straight into Episode 1 of our Flight Reduction - Experiences and Interviews: <font style="font-size:20px"><a href="/assets/flight_exp_asia.pdf" target="_blank"> "A long way to Asia - On a container ship from Malta to Malasia" </a></font>!

You know someone with a similar experience? Is there a person whose story you'd like to share? Let us know at <a href="mailto:development@ssc.ethz.ch" style="color:blue">development@ssc.ethz.ch</a>

<div style="line-height:5px"><br></div>
