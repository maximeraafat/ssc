---
title: Sustainability at D-PHYS
progress-id: 5
modal-id: sust_d-phys

img: anna_knorr_d-phys.png
alt: sustainability at the d-phys
date_:

author: Anna Knörr
description:
---
It must be an incredible feeling when you cross that finish-line after running a marathon. I’ve never run a marathon. And actually, the progress I want to share with you is not a finish-line, either. It’s rather a milestone in the journey towards sustainability of the ETH physics department. During FS20, I was part of a working group in D-PHYS that produced a document thoroughly analysing our CO2 footprint and making suggestions for improvement. It was inspiring to hear how strongly some of our professors, technicians etc. do care about sustainability. How much time and energy they are willing to sacrifice for the sake of making a change, even if they’d rather be doing the research they love.

Please read this <font style="font-size:20px"><a href="/assets/anna_knorr_d-phys.pdf" target="_blank"> article </a></font> I wrote about our work. And return to this page to read about the further progress we make in HS20.

<div style="line-height:5px"><br></div>
