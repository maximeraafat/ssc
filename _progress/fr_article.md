---
title: Flight Reduction Article
progress-id: 6
modal-id: flight_red_art

img: fl_red_article.png
alt: flight reduction article
date_:

author: Theo Ray, Hasan Arkay and Julia Werz
description:
---
<b>Let's change our travel behaviour</b>. This step has one of the biggest potentials to limit the greenhouse gas emissions produced by employees and students of ETH Zürich in the near future.

Avoiding the airplane is, however, generally perceived as restricting international network building and making it harder to keep up personal relationships. Thus, the prospects of reduced or more sustainable mobility are often received with a fear of loss of academic quality.

This article aims to reassure that this does not have to be the case. In the first part, we will give you the <b>necessary numbers and figures</b> to understand the importance of a comprehensive mobility strategy in the development of a sustainable university. Then, we will describe the <b>advantages and challenges of low-emission solutions</b> for necessary international meetings. At the end, we provide a <b>short list of tips</b> to support readers who are willing to take action and build a more sustainable and responsible academic career.

Enjoy reading <font style="font-size:20px"><a href="/assets/flight_reduction_sust.pdf" target="_blank"> "How to avoid airplanes while maintaining international exchange and academic performance?" </a></font> 😊

<div style="line-height:5px"><br></div>
