---
title: Advent, advent...
progress-id: 7
modal-id: advent

img: advent.png
alt: advent
date_:

author: Anna Knörr, Ümmi Armagan
description:
---
Advent is here, the sun is setting on this year's fall semester and it's time to start reflecting on what SSC has achieved in HS20. What comes to my mind immediately? We finally have a proper <b>Communication Team</b>. It makes me so glad.
<br><br>
Perhaps it's a bit hard to empathize with why this is a real achievement from an outside perspective, so let me explain. On the broad level, a central aim of the SSC Retreat in March 2020 was creating a stable long-term structure that would ensure smooth knowledge transfer when members leave upon graduation and would also enable new members to easily join and find a meaningful place. Hence, we grouped ourselves into various Teams. On the concrete level, we realized how important communication is - if no one knows you exist, do you exist? :p Hence, we created our own Communication Team.

Several months onward, experience has confirmed just how valuable that aspect of organizational structure really is. And I'd also emphasize the importance of (online) <i>infra</i>structure, but I'll save the details for a further semester-end-post.

Because the point I really want to get to, is that our Comm Team has something special cooking for you during the last December weeks: It's time for the <b>SSC Adventkalendar</b>! From Mindful Mondays, via Tempting Tuesdays and Factful Fridays all the way to Serious Sundays, we have delicious food for soul and thought in store for you every day. Head straight over to <a href="https://instagram.com/ssc_ethz" target="_blank">Instagram</a> and enjoy.
(And don't worry, our "Sustainable Household Tips" Series will be back to support you in the new year :p)
