---
title: reCIRCLE is here!
progress-id: 4
modal-id: recircle

img: recircle.png
alt: recircle
date_:

author: Anna Knörr
description:
---
What a delicious email. The news@hk.ethz.ch newsletter just arrived in my inbox confirming that from 14. September onwards, we will have the option of using <b>reCircle</b> Tupperware in nearly all* canteens and restaurants on our campus. Even three food trucks are joining the movement! If you buy a takeaway meal, you are no longer forced to use a single-use plastic container destined for the rubbish bin. Instead you can choose to enjoy your meal in a smart-looking purple reBox. <b>Please make that choice.</b>

This step is part of the on-going catering <a href="https://ethz.ch/en/the-eth-zurich/sustainability/campus/environment/food.html" target="_blank"><i>Climate Program</i></a> and was not easy to achieve: I remember the surprising resistance we faced during Climate Program workshops over a year ago. So again, please make that choice. A choice leading away from our current throw-away society towards a more circular economy.

*Let’s convince RiceUp to join as well! Why don’t you sneak in a <i>"Can I use a reBox, please?"</i> next time you have lunch there. 😉

Read more <a href="https://ethz.ch/content/associates/services/de/news-und-veranstaltungen/intern-aktuell/archiv/2020/09/von-wegwerf-zu-mehrweg.html" target="_blank"><i>here</i></a>
