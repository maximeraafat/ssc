---
title: Divest ETH
progress-id: 1
modal-id: divest1

img: divestment_rows.png
alt: divestment
date_:

author: Ulrike Proske, Leon Beck and Nicoletta Brazzola
description:
---

Divesting from <b>fossil fuels</b> is an essential step to combatting climate change. ETH Zurich urgently needs to stop investing in fossil fuels. Only then will this leading institution truly contribute to a greener future, not only with its research and teaching but also its financial budget.
<br><br> Read more on how our Development team is working together with other student organizations in order to #DivestETH in this <a href="https://zs-online.ch/studis-fordern-erdoelfreie-eth-finanzen/" target="_blank"> ZS Article </a> and join us for a first lively <a href="https://ssc.ethz.ch/events/#divestment" target="_blank">Zoom discussion</a> on November 18th.
