---
title: It's Official!
progress-id: 2
modal-id: susu_d-phys

img: anna_knorr_d-phys.png
alt: sustainability at the d-phys
date_:

author: Anna Knörr
description:
---
The course has been set. The physics department officially endorsed the 47 page CO2 analysis and guideline document which I presented in <a href="https://ssc.ethz.ch/progress/#sust_d-phys" target="_blank">a previous post</a> in the departmental conference last Friday (2. October). There were no major objections and the document received a 85% majority support. However, this success will be purely symbolic until we actually put the guidelines into practice. Therefore, we physics students at SSC are rolling up our sleeves together with a handful of professors and the D-PHYS communication staff. Our first project is a communication campaign with the immediate aim of <b>making the content of the document known to students and staff</b> as well as the long-term ambition of <b>kicking off a cultural change at our department</b>. Stay tuned for details!

One more thing: Incidentally, the physics department is being evaluated by an international committee end of October. In preparation, students and staff have filmed video messages with their personal impressions and wishes. Here is what I have to say: <a href="https://polybox.ethz.ch/index.php/s/9yEXWCEalIyYuuQ" target="_blank">"Successful and Sustainable - The New Generation of Researchers"</a>
