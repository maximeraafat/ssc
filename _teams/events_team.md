---
title: Events Team
team-id: 3
modal-id: events

name: nela.jpg
img: events.jpg
alt: events team

about: Our Events Team is an enthusiastic bunch who put their heads together every week to brainstorm and organize both social and intellectual events. Cooking classes, hiking trips, movie screenings, talks, book readings... the formats are diverse! The regular SSC beer is a favourite for newcomers – join us for a (non) alcoholic drink to share your thoughts and ideas or simply to meet new friends with the same green values. <br><br>Have a look at what is coming up <a href="https://ssc.ethz.ch/events" target="_blank">this month</a> or contact <a href="mailto:events@ssc.ethz.ch" style="color:blue">events@ssc.ethz.ch</a> (Nela Foukalova).

description: My name is Nela and I am a Master student in Business and Economics. I joined SSC almost two years ago, and I am very happy to be a part of this great community. Through SSC, I have been able to have a direct impact on what’s happening at ETH and to promote the message of sustainability in the broad sense. I have also learned a lot about diverse topics, joined some interesting discussions, and made friends. Besides my activities in SSC, I am working on my Master thesis, reading books or listening to interesting podcasts, going on walks along the lake, or hiking in the mountains. In light of the current situation, SSC has been able to switch most of its events to online format. This semester, we are happy to be able to continue organising talks, panel discussions, workshops and a quiz night, also in collaboration with experts in various topics. I am looking forward to seeing you at our events!
---
