---
title: President
team-id: 2
modal-id: president

name: anna.jpg
img: president.jpg
alt: president

about: Salut and 你好! My name is Anna, I’m a 3rd year physics Bachelor student who loves not only topological phenomena in condensed matter or the beautiful (and useful!) world of category theory or giving spontaneous mini-lectures about the expanding universe to inquisitive baristas or... :p but also has a green streak burning in her heart! I’ve been president of SSC since March 2020. Bringing all our members' strengths and motivation to play in the SSC symphony fills me with joy. In my free time, you’ll either find me in a handstand, listening to some Trifonov or busy with the D-PHYS Climate Ambassadors 😊 <br> <a href="https://www.danielwinkler.ch/" target="_blank"> Daniel Winkler Fotografie </a>

description: 
---

