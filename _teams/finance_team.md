---
title: Finance Team
team-id: 6
modal-id: finance

name: blandine.jpg
img: finance.jpg
alt: finance team

about: Money, money, money... Yes, SSC also has a budget that wants to be taken care of. Currently, we are funded by VSETH. If, however, you have skills in managing external relations and want to increase our apéro budget, feel free to take on this role and reach out to potential sponsors! <br><br>Contact <a href="mailto:finance@ssc.ethz.ch" style="color:blue">finance@ssc.ethz.ch</a> (Blandine Genet).

description: My name is Blandine and I am working on my Master in Food Science. I joined SSC in 2018 and since then have been active in the Events group. I very much enjoy the atmosphere in the group and everybody's motivation to bring awareness about subjects that are close to our hearts. When I am not studying, or taking care of the SSC budget, I like to walk outside or create my clothes as sustainably as possible.
---
