---
title: Communication Team
team-id: 1
modal-id: communication

name: uemmi.jpg
img: communication.jpg
alt: communication team

about: The Communication Team is responsible for making our work at SSC known to students and staff at ETH. This means designing posters, creating Insta posts, writing a regular newsletter, keeping the website up to date... Being visible and attractive online and on campus is crucial. We aim to make sustainability a core value at ETH and encourage good green habits in everyday routines. <br><br>If you have skills and interest in this area, we’d love to welcome you to the team! Get in touch with <a href="mailto:communication@ssc.ethz.ch" style="color:blue">communication@ssc.ethz.ch</a> (Ümmi Armagan). 

description: Hello! I’m Ümmi, a soon to be chemist graduate (hopefully). When I’m not huddled over a table questioning why I have to do quantum mechanics (I just wanted to make things explode) you can find me (still huddled over my table) satisfying my curiosity with some bioinformatics. Other than that, my moral compass urges me to spread sustainability, social justice and similar ideals! Also I REALLY enjoy cooking some vegan comfort food for the people around me :D
---