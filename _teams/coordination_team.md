---
title: Coordination Team
team-id: 5
modal-id: coordination

name: adrian.jpg
img: coordination.jpg
alt: coordination team

about: What we call the Coordination Team is essentially our vice-president. S*he is responsible for keeping SSC running smoothly, i.e. scheduling board meetings, taking care of administrative duties and organizing internal social events, such as the Vegan Summer Barbeque or the SSC Retreat every semester. <br><br>Contact <a href="mailto:coordination@ssc.ethz.ch" style="color:blue">coordination@ssc.ethz.ch</a> (Adrian Müller).

description: I’m Adrian, 3rd year Bachelor student in mathematics. I joined SSC in 2018/19 and have since then been part of our Events Team. It’s a pleasure to contribute to our commission among so many open-minded and committed people! When I’m not coordinating our Teams in SSC, you’re likely to find me proving theorems in the math library, reading a good novel or just enjoying my student life with my friends.
---
