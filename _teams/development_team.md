---
title: Development Team
team-id: 4
modal-id: development

name: kristof.jpg
img: development.jpg
alt: development team

about: The Development Team is the core engine of SSC when it comes to catalysing institutional change at ETH. Hot topics are chosen according to the interests of each team member. Currently, we emphasize flight reduction, divestment and food. We work closely with the official office <a href="https://ethz.ch/en/the-eth-zurich/sustainability.html" target="_blank">ETH Sustainability</a>, are in frequent contact with various ETH bodies (e.g. ETH Mobility Platform, SGU, Environmental Commission...) and keep developing our network with research institutions (e.g. World Food Systems Center), ETH spinoffs (e.g. myclimate) and other Swiss student organizations (e.g. VSS-VSN). <br><br>Feeling inspired? Contact <a href="mailto:development@ssc.ethz.ch" style="color:blue">development@ssc.ethz.ch</a> (Kristóf Sárosi).

description: My name is Kristóf and I am a PhD student in D-MAVT at ETH. I joined SSC at the end of HS2019 as I wanted to work on reducing food waste on campus. It is a personal topic for me as if it was not for RestEssBar Winti, I would have struggled much more in my first year in Switzerland. I joined the Development team and later formed a subgroup handling food related projects. This year, I coordinate the Development team to support our members to reach the goals of their visionary projects leading to a more sustainable future at ETH. If you are an early bird, you can find me in a rowing boat on Zürisee, hiking or running regardless of the weather but most probably spending unhealthy amount of time exploring the world of music.
---
