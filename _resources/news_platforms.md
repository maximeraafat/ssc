---
title: Knowledge Platforms
resource-id: 2
modal-id: news_platforms
news: true

img: mandela.png
alt: news

description:
---

<a href="https://sdgs.un.org/goals" target="_blank">SDG Knowledge Platform</a> – all you need to know about the 17 Sustainable Development Goals

<hr style="border-top: solid 1px; max-width: 75%">

International Institute for Sustainable Development (<a href="https://www.iisd.org/" target="_blank">IISD</a>) – well-written knowledge base, blog articles and podcasts plus recent research from this impactful think tank.

<hr style="border-top: solid 1px; max-width: 75%">

<a href="https://www.azimuthproject.org/azimuth/show/John+Baez" target="_blank">John Baez & The Azimuth Project</a> – inspiration for scientists, engineers and mathematicians.

<hr style="border-top: solid 1px; max-width: 75%">

Main-stream newspapers with nice climate digests: The Guardian, NY Times
