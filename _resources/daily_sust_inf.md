---
title: Sustainably Informed
resource-id: 2
modal-id: daily_sust_inf
daily: true

img: sust_inf.png
alt: book

description:
---

Read informative articles written by SSC members that tell you which impactful changes you can make to your everyday life as a student.

<ul class="list-inline">
  <li><font style="font-size:20px"><a href="/assets/Plastic_In_Our_Daily_Lives.pdf" target="_blank">Plastic In Our Daily Lives</a></font></li><br>
  <li><font style="font-size:20px"><a href="/assets/Christmas_Survival_Guide.pdf" target="_blank">The Sustainable Christmas Survival Guide</a></font></li><br>
<ul>
