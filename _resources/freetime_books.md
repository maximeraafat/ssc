---
title: Books
resource-id: 1
modal-id: freetime_books
freetime: true

img: book.jpg
alt: book
email: events@ssc.ethz.ch

description:
---

Join our reading group if you’d like to share your thoughts! (<a href="mailto:events@ssc.ethz.ch" style="color:blue">events@ssc.ethz.ch</a>)
<br><br>

<ul class="list-inline">
  <li><b>The Sane Society</b>, Erich Fromm</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>Having or Being?</b>, Erich Fromm</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>Digital Minimalism</b>, Cal Newport</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>Fashionopolis: The price of Fast Fashion and the Future of Clothes</b>, Dana Thomas</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>The Climate of Hope</b>, Michael Bloomberg and Carl Pope</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>The Hidden Life of Trees</b>, Peter Wohlleben</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>Sunlight and Seeweed </b>, Tim Flannery</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>This Changes Everything </b>, Naomi Klein</li>
</ul>
