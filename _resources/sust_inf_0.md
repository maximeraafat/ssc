---
title: Subject 0 - Christmas
resource-id: 0
modal-id: sust_inf_0
sust_inf: false # set this to false once website online
sitemap: false # this page is a demo that should not be added to the sitemap

img: christmas.jpg
alt: christmas

author: Maxime Raafat
description: This is our first <i>Sustainably Informed</i> subject!
---

This text serves as an example.

Here comes a lot of text, with <b>bold text</b>, or maybe <i>italic text</i>.
Sometimes, I also want to change the color in <font color="blue">blue</font>, or
apply a different <font face="Brush Script Std, cursive">font</font>.

I can even apply all those properties at <font color="blue" face="Brush Script Std, cursive"><b><i>once</i></b></font>.

I can add an image here, and even link this <a href="/img/home/mountain_smiles.jpg" alt="image description" target="_blank">word</a> to my image.

<img src="/img/home/mountain_smiles.jpg" class="img-responsive img-centered" alt="image description">
