---
title: D-PHYS
resource-id: 3
modal-id: sust_eth_dphys
sust_eth: true

img: dphys.png
alt: dphys

description:
---

In FS20, the Physics Department decided to seriously start making an effort - head straight over to this <a href="https://www.phys.ethz.ch/the-department/sustainability.html" target="_blank"> official website</a> to find out all details and updates!

Also, feel free to read our two PROGRESS updates: 
<a href="https://ssc.ethz.ch/progress/#sust_d-phys" target="_blank">Sustainability at D-PHYS</a> (September 2020) and <a href="https://ssc.ethz.ch/progress/#susu_d-phys" target="_blank">It's Official!</a> (October 2020).
