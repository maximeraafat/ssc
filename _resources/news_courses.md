---
title: Online Courses
resource-id: 1
modal-id: news_courses
news: true

img: gandhi.png
alt: news

description:
---

Excellent <a href="https://courses.edx.org/courses/course-v1:SDGAcademyX+CCSN001+3T2019/1b09562ee7824c908ac6b2d0e0b5331d/" target="_blank">EdX Course</a> on "Climate Change Science and Negotiations"

<hr style="border-top: solid 1px; max-width: 75%">

Want more courses? Check out the <a href="https://unccelearn.org/" target="_blank">UN CC:Learn Platform</a>.
