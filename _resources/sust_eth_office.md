---
title: ETH Sustainability (Official Office)
resource-id: 1
modal-id: sust_eth_office
sust_eth: true

img: mission.png
alt: mission

description:
---

ETH Sustainability is the official office at our university with dedicated employees that work closely with our student commission SSC. We meet every first Monday of the month to exchange student and staff perspectives. In the organizational structure of ETH Zurich, ETH Sustainability is embedded in the Office of the President.

They maintain this <a href="https://ethz.ch/en/the-eth-zurich/sustainability.html" target="_blank">website</a> with links to our university’s research, talks, official news and more.

Furthermore, they maintain a <a href="https://ethz.ch/en/the-eth-zurich/sustainability/education/sustainable-vvz.html" target="_blank">sustainability course catalog</a>.
