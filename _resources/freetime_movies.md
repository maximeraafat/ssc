---
title: Movies
resource-id: 2
modal-id: freetime_movies
freetime: true

img: movies.png
img-modal: movie_collage.png
alt: movie

description:
---

Feeling like a relaxed evening after an intense university week? Can’t wait for the next SSC movie night? Here’s some quality material for you to watch online:
<br><br>

<ul class="list-inline">
  <li><b>Cowspiracy</b> (Netflix) - The change we need in agriculture for more sustainable handling of resources</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>Before The Flood</b> (Disney+) - Leonardo DiCaprio on a journey as a United Nations Messenger of Peace</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>The Game Changers</b> (Netflix) - Top athletes see the potential of vegan nutrition</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>Inconvenient Truth</b> (Internet) - Former United States Vice President Al Gore's campaign to educate people about global warming</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>Tomorrow</b> (Internet) - An optimistic journey of concrete solutions to mitigate climate change</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>Snowpiercer</b> (Internet) - Action movie in a train that carries the last remnants of humanity after an attempt to stop global warming</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>Our Planet</b> (Internet) - Experience the beauty of our planet and what we stand to lose due to climate change</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>Sustainable</b> (Internet) - Americas food and farming system in a sustainability perspective</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>Fast Fashion</b> (Internet) - The consequences of our clothes-buying habits</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>The True Cost</b> (Internet) - Film-Producer Andrew Morgan travels around the globe to visit the people who set the tone for modern fashion</li>
  <hr style="border-top: solid 1px; max-width: 50%">

  <li><b>Avatar</b> (Internet) - A unique mission on the moon Pandora with Avatars</li>
</ul>
