---
title: Sustainable Alternative Solutions Map
resource-id: 2
modal-id: map_alternative
map: true

img: map_alternative.png
alt: sustainable alternative solutions map

description: Have a look at the <a href="http://www.transition-zuerich.ch/wp-content/uploads/2018/09/qk-final-front.pdf" target="_blank"><i>sustainable alternative solutions map</i></a> from <a href="http://www.transition-zuerich.ch" target="_blank"><i>Transition Zürich</i></a>, and its <a href="http://www.transition-zuerich.ch/wp-content/uploads/2018/09/qk-final-back_copy.pdf" target="_blank">back</a> to see the map's legend.
link: http://www.transition-zuerich.ch/wp-content/uploads/2018/09/qk-final-front.pdf
---
