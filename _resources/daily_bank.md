---
title: Bank Account
resource-id: 1
modal-id: daily_bank
daily: true

img: abs.jpg
alt: bank

description:
---

Align your money with your values.

Open an account ("Ausbildungskonto") at the <b>Alternative Bank Schweiz</b> <a href="https://www.abs.ch/de/privatpersonen/fuer-den-alltag/ausbildungskonto/" target="_blank">online</a> or in person at <i>Kalkbreitestrasse 10, 8036 Zürich</i>.
