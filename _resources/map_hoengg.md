---
title: Sustainable Höngg Map
resource-id: 3
modal-id: map_hoengg
map: true

img: map_hoengg.png
alt: sustainable höngg map

description: Have a look at the <a href="http://www.transition-zuerich.ch/wp-content/uploads/2019/06/Karte-Ho%CC%88ngg-kompr.pdf" target="_blank"><i>Höngg-district map</i></a> from <a href="http://www.transition-zuerich.ch" target="_blank"><i>Transition Zürich</i></a>.
link: http://www.transition-zuerich.ch/wp-content/uploads/2019/06/Karte-Ho%CC%88ngg-kompr.pdf
---
