---
title: Sustainable Buchegg Map
resource-id: 1
modal-id: map_buchegg
map: true

img: map_buchegg.png
alt: sustainable buchegg map

description: Have a look at the <a href="http://www.transition-zuerich.ch/wp-content/uploads/2019/06/Karte-Buchegg-kompr1.pdf" target="_blank"><i>Buchegg-district map</i></a> from <a href="http://www.transition-zuerich.ch" target="_blank"><i>Transition Zürich</i></a>.
link: http://www.transition-zuerich.ch/wp-content/uploads/2019/06/Karte-Buchegg-kompr1.pdf
---
