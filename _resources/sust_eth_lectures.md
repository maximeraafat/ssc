---
title: ETH Lectures and Degrees
resource-id: 2
modal-id: sust_eth_lectures
sust_eth: true

img: greenvvz.png
alt: greenvvz

description:
---

You can find the ETH sustainability course catalog <a href="https://ethz.ch/en/the-eth-zurich/sustainability/education/sustainable-vvz.html" target="_blank">here</a>. 

A sample of ETH Master degrees focused on sustainability are as following:
<ul class="list-inline">
  <li><a href="https://iac.ethz.ch/edu/prospective.html" target="_blank">Atmospheric and Climate Science</a></li><br>
  <li><a href="https://master-energy.ethz.ch/" target="_blank">Energy Science and Technology</a></li><br>
  <li><a href="https://ethz.ch/en/studies/master/degree-programmes/architecture-and-civil-engineering/integrated-building-systems.html" target="_blank">Integrated Building Systems</a></li><br>
  <li><a href="https://istp.ethz.ch/education/MSc-ETH-STP.html" target="_blank">Science, Technology and Policy</a></li><br>
</ul>
